#!/usr/bin/python

#Written as a final project for CNA473
#Do not actually try to use this software for real
#Hashing files over and over is horribly inefficient

import hashlib
import os
import sys
import time

def main():
	arguments = sys.argv[1:]
	if not argumentCheck(arguments):
		return 1
	logFile = arguments[-1]
	directories =  arguments[0:len(arguments)-1]
	
	print "Monitoring integrity of: " + str(directories)
	print "Logging to " + logFile
	initialHashes = createInitalHash(directories)
	
	while(True):
		time.sleep(10)
		checkHashes(initialHashes, directories, logFile)

#Input validation function
def argumentCheck(arguments):
	startMessage = "Started checking directories {} at time {}\n"
	if not arguments:
		print "Usage: " + sys.argv[0] + " directories [...] logfile"
		return False
		
	try:
		arguments[-2]  #test if at least 2 arguments were specified
		logFile = arguments[-1]  #Set last argument to be the log file
		directories =  arguments[0:len(arguments)-1]
	except IndexError:
		print "You must specify at least one directory to monitor and an output file"
		return False
		
	for directory in directories:
		if not os.path.exists(directory):
			print "File/Directory " + directory + " does not exist"
			return False
		
		if os.path.isdir(directory):
			for root, subdirs, files in os.walk(directory):
				for file in files:
					filename = root + "/" + file
					if not os.access(filename, os.R_OK):  #File is not readable
						print "Permission denied trying to read file " + filename + " from directory " + directory
						return False
						
		elif os.path.isfile(directory):
			filename = directory
			if not os.access(filename, os.R_OK):  #File is not readable
				print "Permission denied trying to read file " + filename
				return False
			
	try:
		with open(logFile, "a+") as openLogFile:
			openLogFile.write(startMessage.format(str(directories), time.asctime()))
	except:
		print "You don't have permission to write to" + logFile
		return False
	
	return True
		
def createInitalHash(directories):
	initialHashes = {}
	for directory in directories:
		if os.path.isdir(directory):
			for root, subdirs, files in os.walk(directory):
				for file in files:
					filename = root + "/" + file
					with open(filename, 'rb') as afile:
						data = afile.read()
						hash = hashlib.md5(data).hexdigest()
						initialHashes[filename] = hash
		elif os.path.isfile(directory):  #in this case, directory is just a single file
			filename = directory
			with open(filename, 'rb') as afile:
				data = afile.read()
				hash = hashlib.md5(data).hexdigest()
				initialHashes[filename] = hash
			
	return initialHashes

def printHashes(initialHashes):
	for file in initialHashes:
		print file
		print initialHashes[file] + "\n"
		
def checkHashes(initialHashes, directories, logFile = "/var/log/fim.log"):
	for directory in directories:
		if os.path.isdir(directory):
			for root, subdirs, files in os.walk(directory):
				for file in files:
					filename = root + "/" + file
					with open(filename, 'rb') as afile:
						data = afile.read()
						hash = hashlib.md5(data).hexdigest()
						
						#Check if a file has been added
						if filename not in initialHashes.keys():
							initialHashes[filename] = hash
							writeMessage(filename, "added", logFile)
						
						#Check if a file has been modified
						if initialHashes[filename] != hash:
							initialHashes[filename] = hash
							writeMessage(filename, "changed", logFile)
							
		elif os.path.isfile(directory):
			filename = directory
			with open(filename, 'rb') as afile:
				data = afile.read()
				hash = hashlib.md5(data).hexdigest()
						
				#Check if a file has been added
				if filename not in initialHashes.keys():
					initialHashes[filename] = hash
					writeMessage(filename, "added", logFile)
						
				#Check if a file has been modified
				if initialHashes[filename] != hash:
					initialHashes[filename] = hash
					writeMessage(filename, "changed", logFile)
			
					
	#Check if a file has been deleted
	for filename in initialHashes.keys():
		if not os.path.exists(filename):
			del initialHashes[filename]
			writeMessage(filename, "removed", logFile)

def writeMessage(filename, messageType, logFile):
	message = "File {} was {} at time {}"
	with open(logFile, "a+") as logfile:
		logfile.write(message.format(filename, messageType, time.asctime() + "\n"))
	print message.format(filename, messageType, time.asctime())
	
if __name__ == "__main__":
	main()
